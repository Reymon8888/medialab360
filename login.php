<?php
   include("data/database.php");
   session_start();

   $error = "";
   
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      
      $myusername = mysqli_real_escape_string($connection,$_POST['correo']);
      $mypassword = mysqli_real_escape_string($connection,$_POST['contrasena']); 
      
      $sql = "SELECT * FROM usuarios WHERE correo = '$myusername' and contrasena = '$mypassword'";
      $result = mysqli_query($connection,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      //$active = $row['active'];
      
      $count = mysqli_num_rows($result);
      
      // If result matched $myusername and $mypassword, table row must be 1 row
		
      if($count == 1) {
         //session_register("usuario");
         $_SESSION['usuario'] = $myusername;
         $_SESSION['tipo'] = $row['tipo'];

         if($row['tipo']==="Administrador"){
          header("location: dashboard.php");
         }
         else if($row['tipo']==="Usuario"){
          header("location: mi-perfil.php");
         }
         
      }else {
         $error = "Usuario o contraseña incorrectos";
      }
   }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Grupo Media Lab</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
    
  <link rel="icon" href="img/logos/logox2.png" type="image/png" />
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300&family=Manrope:wght@200&display=swap" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: DevFolio
    Theme URL: https://bootstrapmade.com/devfolio-bootstrap-portfolio-html-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>
<style>
  body{
    cursor: url('img/cursor.png'), auto;
  }
</style>
<body id="page-top">

  <!--/ Nav Star /-->
  <nav class="navbar navbar-b navbar-trans navbar-expand-md fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll" href="#page-top"><img width="70px" src="img/logos/logox2.png" alt=""></a>
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link js-scroll active" href="index.php">Inicio</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll active" href="registro.php">Registrarse</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!--/ Nav End /-->

  <!--/ Intro Skew Star /-->
  <div id="home" class="intro route bg-image" style="background-color:#fff;">
    <div class="overlay-itro"></div>
    <div class="intro-content display-table">
      <div class="table-cell">
        <div class="container"><br><br>
            <div class="row">
                <div class="col-sm-12">
                  <div class="contact-mf">
                    <div id="contact" class="box-shadow-full">
                      <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                          <div class="title-box-2">
                            <h5 class="title-left">
                              Iniciar sesión
                            </h5><br>
                            <img src="img/logos/logox2.png" width="30%" alt="">
                          </div>
                          <div>
                              <form action="" method="POST" role="form" class="">
                              <div id="errormessage"></div>
                              <label for="" class="text-danger"><?php echo $error ?></label>
                              <div class="row">
                                <div class="col-md-12 mb-3">
                                  <div class="form-group">
                                    <input type="text" name="correo" class="form-control" id="name" placeholder="Correo" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres" />
                                    <div class="validation"></div>
                                  </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                  <div class="form-group">
                                    <input type="password" class="form-control" name="contrasena" id="email" placeholder="Contraseña" data-rule="minlen:4" data-msg="Contraseña no válida" />
                                    <div class="validation"></div>
                                  </div>
                                </div><!-- 
                                <div class="col-md-12 mb-3">
                                    <div class="form-group">
                                      <input type="text" class="form-control" name="subject" id="subject" placeholder="Asunto" data-rule="minlen:4" data-msg="Ingresa al menos 8 caracteres" />
                                      <div class="validation"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                  <div class="form-group">
                                    <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Escribe tu mensaje" placeholder="Mensaje"></textarea>
                                    <div class="validation"></div>
                                  </div>
                                </div> -->
                                <div class="col-md-12">
                                  <button type="submit" class="button button-a button-big button-rouded">Ingresar</button>
                                </div>
                                <div class="col-md-12">
                                 <a class="text-primary"  href="pass.php">Olvidé mi contraseña</a> 
                                  <br><br></div>  
                              </div>
                              </div>
                            </form>
                          </div>
                        </div>
                        <div class="col-md-3"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        </div>
      </div>
    </div>
  </div>
  <!--/ Intro Skew End /-->

  <!--/ Section Services Star /-->
  
  <!--/ Section Services End /-->

  <!--/ Section Portfolio Star /-->
  
  <!--/ Section Portfolio End /-->

  <!--/ Section Testimonials Star /-->

  <!--/ Section Blog Star /-->
 
  <!--/ Section Blog End /-->

  <!--/ Section Contact-Footer Star /-->
  <section class="paralax-mf footer-paralax bg-image sect-mt4 route" style="background-image: url(img/overlay-bg.jpg)">
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="copyright-box">
                <p class="copyright" style="color:#000;">&copy; Copyright <strong>Media Lab 360</strong>. All Rights Reserved</p>
              <div class="credits">
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </section>
  <!--/ Section Contact-footer End /-->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader" align="center" class="justify-content-center">
    <img src="img/logos/logox2.png" style="margin-top:40%;" width="200px" height="140px" alt="">
  </div>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/popper/popper.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/counterup/jquery.waypoints.min.js"></script>
  <script src="lib/counterup/jquery.counterup.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <script src="lib/typed/typed.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
