<?php

include('data/database.php');
   session_start();

   $imgPred = '';
   
   $user_check = $_GET['actorId'];
   $tipo = $_SESSION['tipo'];
   
   
   $ses_sql = mysqli_query($connection,"select * from usuarios where id = '$user_check' ");
   
   $row = mysqli_fetch_array($ses_sql,MYSQLI_ASSOC);
   $idUsuario = $row['id'];
   $usuario = $row['nombre'];
   
   $login_session = $row['correo'];
   
   if(!isset($_SESSION['usuario'])){
    header("location:login.php");
    die();   
   }else if($_SESSION['tipo'] !== 'Administrador'){
    header("location:login.php");
    die();  
   }

   $mensaje_imagenes = '';
   

    $query_imagenes = "SELECT * FROM imagenes WHERE usuario_id = '$idUsuario' AND estatus = 1 AND img_pred != 1 ORDER BY id ASC";
    $imagenes = mysqli_query($connection, $query_imagenes);
    $row_imagenes = mysqli_fetch_array($imagenes, MYSQLI_ASSOC);
    $totalRows_imagenes = mysqli_num_rows($imagenes);

   /* $pro = $_POST['producto'];
   $cat = $_POST['categoria'];
   $old = $_POST['old'];
   $oldImg = $_POST['img'];
   $img = $pro.'.jpg'; */

   /* if($pro !== $old){
       rename('<?php echo $fileDir; ?>'.$oldImg, '<?php echo $fileDir; ?>'.$img);
   } */
   $valida_img_pred ="SELECT * FROM imagenes WHERE usuario_id = '$idUsuario' AND img_pred = 1";
    $image_pred = mysqli_query($connection, $valida_img_pred);
    $imagPred = mysqli_fetch_array($image_pred, MYSQLI_ASSOC);
    $result_img_pred = mysqli_num_rows($image_pred);

    if($result_img_pred!= 0){
        $imgPred = $fileDir.$idUsuario.'/'.$imagPred['imagen'];
    }else{
        $imgPred = 'img/no-image-user.png';
    }

    if($result_img_pred === 0){
      $mensaje_imagenes = 'El actor aún no ha agregado imágenes';
    }else if($totalRows_imagenes === 0){
      $mensaje_imagenes = 'El actor sólo ha agregado una imagen';
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?php echo $row['nombre'].' '.$row['apellido_pat'].' '.$row['apellido_mat']; ?></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
    
  <link rel="icon" href="img/logos/logox2.png" type="image/png" />
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300&family=Manrope:wght@200&display=swap" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: DevFolio
    Theme URL: https://bootstrapmade.com/devfolio-bootstrap-portfolio-html-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>
<style>
  body{
    cursor: url('img/cursor.png'), auto;
  }
</style>
<body id="page-top">

  <!--/ Nav Star /-->
  <nav class="navbar navbar-b navbar-trans navbar-expand-md fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll" href="#page-top"><img width="70px" src="img/logos/logox2.png" alt=""></a>
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link js-scroll active" href="index.php">Inicio</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link js-scroll" href="#work">Vitrina del cliente</a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link js-scroll" href="dashboard.php">Administración</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll" href="logout.php">Cerrar sesión</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!--/ Nav End /-->

  <!--/ Intro Skew Star /-->
  
<div class="overlay-itro"></div>
<section id="work" class="portfolio-mf sect-pt4 route" style="background-color:#fff;">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="title-box text-center">
            <h3 class="title-a"><br><br>
              <?php echo $row['nombre'].' '.$row['apellido_pat'].' '.$row['apellido_mat']; ?>
            </h3>
            <div class="line-mf"></div><br>
            <h6>Id: <strong><?php echo $row['id']; ?></strong></h6><br>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="work-box">
            <a href="<?php echo $imgPred; ?>" data-lightbox="gallery-mf">
              <div class="work-img">
                <img src="<?php echo $imgPred; ?>" width="100%" alt="" class="img-fluid">
              </div>
              <div class="work-content">
                <div class="row">
                  <div class="col-sm-12 text-center">
                    <h2 class="w-title">Foto principal</h2>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-8">
        <table class="table table-responsive table-striped">
            <thead>
                <tr class="text-center">
                    <th colspan="3">Información general</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <strong>Fecha de nacimiento: </strong><?php echo $row['fecha_nacimiento'] ?><br>
                        <strong>Nacionalidad: </strong><?php echo $row['pais_nacimiento'] ?><br>
                        <strong>Ciudad actual: </strong><?php echo $row['ubicacion_actual'] ?><br> 
                    </td>
                    <td>
                        <strong>Género: </strong><?php echo $row['genero'] ?><br>
                        <strong>Edad: </strong><?php echo $row['edad'] ?> años<br>
                        <strong>Estatura: </strong><?php echo $row['estatura'] ?> cm<br>
                        <strong>Peso: </strong><?php echo $row['peso'] ?> Kg<br>
                        <strong>Complexión: </strong><?php echo $row['complexion'] ?><br>
                        <strong>Aspecto étnico: </strong><?php echo $row['apariencia_etnica'] ?><br>
                        <strong>Color de piel: </strong><?php echo $row['color_piel'] ?><br>
                    </td>
                    <td>
                        <strong>Color de ojos: </strong><?php echo $row['color_ojos'] ?><br>
                        <strong>Color de cabello: </strong><?php echo $row['color_cabello'] ?><br>
                        <strong>Corte de cabello: </strong><?php echo $row['largo_cabello'] ?><br>
                        <strong>Estilo de cabello: </strong><?php echo $row['estilo_cabello'] ?><br>
                        <strong>Tatuajes: </strong><?php if($row['tatuajes']==''){echo 'Ninguno';}else{ echo $row['tatuajes'];}; ?><br>
                        <strong>Piercings: </strong><?php if($row['piercings']==''){echo 'Ninguno';}else{ echo $row['piercings'];}; ?><br>
                    </td>
                </tr>
            </tbody>
            <thead>
                <tr class="text-center">
                    <th colspan="3">Otros datos</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <strong>Estilos de baile: </strong><?php if($row['danzas']==''){echo 'Ninguno';}else{ echo $row['danzas'];}; ?><br>
                        <strong>Deportes: </strong><?php if($row['deportes']==''){echo 'Ninguno';}else{ echo $row['deportes'];}; ?><br>
                    </td>
                    <td>
                        <strong>Música: </strong><?php if($row['instrumento']==''){echo 'No';}else{ echo $row['instrumento'];}; ?><br>
                        <strong>Idiomas: </strong><?php if($row['idiomas']==''){echo 'Sin información';}else{ echo $row['idiomas'];}; ?><br>
                    </td>
                    <td>
                        <strong>Realiza desnudos: </strong><?php if($row['desnudo']==1){echo 'Si';}else{ echo 'No';};  ?><br>
                    </td>
                </tr>
            </tbody>
            <thead>
                <tr class="text-center">
                    <th>Contacto</th>
                    <th colspan="2">Más sobre <?php echo $row['nombre']; ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <strong>Correo: </strong><?php echo $row['correo'] ?><br>
                        <strong>Teléfono: </strong><?php echo $row['telefono1'] ?><br>
                    </td>
                    <td colspan="2">
                        <?php echo $row['descripcion']; ?><br>
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
        <?php
        if($totalRows_imagenes != 0){
            do {
          ?>
        <div class="col-md-2">
          <div class="work-box">
            <a href="<?php echo $fileDir.$idUsuario.'/'.$row_imagenes['imagen']; ?>" data-lightbox="gallery-mf">
              <div class="work-img">
                <img src="<?php echo $fileDir.$idUsuario.'/'.$row_imagenes['imagen']; ?>" alt="" class="img-fluid img-size">
              </div>
            </a>
          </div>
        </div>
        <?php
        } while ($row_imagenes = mysqli_fetch_assoc($imagenes));
      }  ?>
        <div class="col-md-12 text-center">
          <h3>
                <?php echo $mensaje_imagenes; ?> 
            </h3><br>
        </div>
        
      </div>
    </div>
  </section>
    
  

  <!--/ Section Portfolio End /-->

  <!--/ Section Contact-footer End /-->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader" align="center" class="justify-content-center">
    <img src="img/logos/logox2.png" style="margin-top:40%;" width="200px" height="140px" alt="">
  </div>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/popper/popper.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/counterup/jquery.waypoints.min.js"></script>
  <script src="lib/counterup/jquery.counterup.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <script src="lib/typed/typed.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
