var mensaje = '';



function validar() {
    //obteniendo el valor que se puso en el campo text del formulario
    //la condición

    var correo = document.getElementById("correo").value;
    var tel1 = document.getElementById("tel1").value;
    var contrasena = document.getElementById("contrasena").value;
    var contrasena2 = document.getElementById("contrasena2").value;

    var nombre = document.getElementById("nombre").value;
    var apellidoPat = document.getElementById("apellidoPat").value;
    var apellidoMat = document.getElementById("apellidoMat").value;
    var fechaNacimiento = document.getElementById("fechaNacimiento").value;
    var paisNacimiento = document.getElementById("paisNacimiento").value;
    var ubicacionActual = document.getElementById("ubicacionActual").value;
    var nombreContacto = document.getElementById("nombreContacto").value;
    var parentesco = document.getElementById("parentesco").value;
    var tel2 = document.getElementById("tel2").value;

    var genero = document.getElementById("genero").value;
    var edad = document.getElementById("edad").value;
    var estatura = document.getElementById("estatura").value;
    var peso = document.getElementById("peso").value;
    var complexion = document.getElementById("complexion").value;
    var aparienciaEtnica = document.getElementById("aparienciaEtnica").value;
    var colorPiel = document.getElementById("colorPiel").value;
    var colorOjos = document.getElementById("colorOjos").value;
    var colorCabello = document.getElementById("colorCabello").value;
    var largoCabello = document.getElementById("largoCabello").value;
    var estiloCabello = document.getElementById("estiloCabello").value;
    //PASO 1
    if (
        correo.length === 0 || /^\s+$/.test(correo) || correo == "" ||
        tel1.length === 0 || /^\s+$/.test(tel1) || tel1 == "" ||
        contrasena.length === 0 || /^\s+$/.test(contrasena) || contrasena == ""
        ) {
        document.getElementById('alert1').style.display="";
        return false;
    }else{
        document.getElementById('alert1').style.display="none";
    }
    if (
        nombre.length === 0 || /^\s+$/.test(nombre) || nombre == "" ||
        apellidoPat.length === 0 || /^\s+$/.test(apellidoPat) || apellidoPat == "" ||
        apellidoMat.length === 0 || /^\s+$/.test(apellidoMat) || apellidoMat == "" ||
        fechaNacimiento.length === 0 || /^\s+$/.test(fechaNacimiento) || fechaNacimiento == "" ||
        paisNacimiento.length === 0 || /^\s+$/.test(paisNacimiento) || paisNacimiento == "" ||
        ubicacionActual.length === 0 || /^\s+$/.test(ubicacionActual) || ubicacionActual == "" ||
        nombreContacto.length === 0 || /^\s+$/.test(nombreContacto) || nombreContacto == "" ||
        parentesco.length === 0 || /^\s+$/.test(parentesco) || parentesco == "" ||
        tel2.length === 0 || /^\s+$/.test(tel2) || tel2 == "" 
        ) {
        document.getElementById('alert2').style.display="";
        return false;
    }else{
        document.getElementById('alert2').style.display="none";
    }
    if (
        genero.length === 0 || /^\s+$/.test(genero) || genero == "" ||
        edad.length === 0 || /^\s+$/.test(edad) || edad == "" ||
        estatura.length === 0 || /^\s+$/.test(estatura) || estatura == "" ||
        peso.length === 0 || /^\s+$/.test(peso) || peso == "" ||
        complexion.length === 0 || /^\s+$/.test(complexion) || complexion == "" ||
        aparienciaEtnica.length === 0 || /^\s+$/.test(aparienciaEtnica) || aparienciaEtnica == "" ||
        colorPiel.length === 0 || /^\s+$/.test(colorPiel) || colorPiel == "" ||
        colorOjos.length === 0 || /^\s+$/.test(colorOjos) || colorOjos == "" ||
        colorCabello.length === 0 || /^\s+$/.test(colorCabello) || colorCabello == "" ||
        largoCabello.length === 0 || /^\s+$/.test(largoCabello) || largoCabello == "" ||
        estiloCabello.length === 0 || /^\s+$/.test(estiloCabello) || estiloCabello == ""
        ) {
        document.getElementById('alert3').style.display="";
        return false;
    }else{
        document.getElementById('alert3').style.display="none";
    }
    if(contrasena != contrasena2) {
        document.getElementById('noMatch').style.display="";
        return false;
        
      } else {
        document.getElementById('noMatch').style.display="none";
      }
    console.log(correo);

}

function paso1(){
let paso1 = document.getElementById('paso1');
let elementStyle = window.getComputedStyle(paso1);
let elementColor = elementStyle.getPropertyValue('color');
if(elementColor !== "rgb(153, 153, 153)" ){
document.getElementById('paso1').style.color="#999";
}else{
document.getElementById('paso1').style.color="#5AB50A";
document.getElementById('paso2').style.color="#999";
document.getElementById('paso3').style.color="#999";
document.getElementById('paso4').style.color="#999";
}
console.log(elementColor);
}


function paso2(){
let paso2 = document.getElementById('paso2');
let elementStyle = window.getComputedStyle(paso2);
let elementColor = elementStyle.getPropertyValue('color');
if(elementColor !== "rgb(153, 153, 153)" ){
document.getElementById('paso2').style.color="#999";
}else{
document.getElementById('paso1').style.color="#999";
document.getElementById('paso2').style.color="#5AB50A";
document.getElementById('paso3').style.color="#999";
document.getElementById('paso4').style.color="#999";
}
}
function paso3(){
let paso3 = document.getElementById('paso3');
let elementStyle = window.getComputedStyle(paso3);
let elementColor = elementStyle.getPropertyValue('color');
if(elementColor !== "rgb(153, 153, 153)" ){
document.getElementById('paso3').style.color="#999";
}else{
document.getElementById('paso1').style.color="#999";
document.getElementById('paso2').style.color="#999";
document.getElementById('paso3').style.color="#5AB50A";
document.getElementById('paso4').style.color="#999";
}
}
function paso4(){
let paso4 = document.getElementById('paso4');
let elementStyle = window.getComputedStyle(paso4);
let elementColor = elementStyle.getPropertyValue('color');
if(elementColor !== "rgb(153, 153, 153)" ){
document.getElementById('paso4').style.color="#999";
}else{
document.getElementById('paso1').style.color="#999";
document.getElementById('paso2').style.color="#999";
document.getElementById('paso3').style.color="#999";
document.getElementById('paso4').style.color="#5AB50A";
}
}
function changeColor(){
    let search = document.getElementById('search');
    let elementStyleSearch = window.getComputedStyle(search);
    let elementColorSearch = elementStyleSearch.getPropertyValue('color');
    
    if(elementColorSearch != "rgb(153, 153, 153)"){
        document.getElementById('search').style.color="#999";
    }else{
        document.getElementById('search').style.color="#5AB50A";
    }

}

