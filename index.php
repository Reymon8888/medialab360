<?php

include('data/database.php');
   session_start();
   
   
   $logeado = false;
   $adminOk = false;
   $usuarioOk = false;
   if(!isset($_SESSION['usuario'])){
    $logeado = false;
   }else{
     if($_SESSION['tipo'] === 'Administrador'){
       $adminOk = true;
     }else if($_SESSION['tipo'] === 'Usuario'){
      $usuarioOk = true;
     }
     $logeado = true;
   }

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Grupo Media Lab</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="Agencia de actores, agencia de talento, actores, talento, seleccion de talento, registro de actores, casting" name="keywords">
  <meta content="Agencia de talento artístico. Actores, extras, modelos, personajes." name="description">

  <!-- Favicons -->
    
  <link rel="icon" href="img/logos/logox2.png" type="image/png" />
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300&family=Manrope:wght@200&display=swap" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<style>
  body{
    cursor: url('img/cursor.png'), auto;
  }
</style>
<body id="page-top">
<script>
    $('.carousel').carousel({
  interval: 11000
})

  </script>
  <!--/ Nav Star /-->
  <nav class="navbar navbar-b navbar-trans navbar-expand-md fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll" href="#page-top"><img width="70px" src="img/logos/logox2.png" alt=""></a>
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link js-scroll active" href="#home">Inicio</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll" href="#nosotros">Acerca de nosotros</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll" href="#blog">Nuestro servicio</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll" href="#contact">Contacto</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll" href="<?php 
            if($adminOk === true){ echo 'dashboard.php'; }else if($usuarioOk === true){echo 'mi-perfil.php';}else{echo 'login.php';}; ?>">
              <?php 
              if($adminOk === true){ echo 'Administración'; }else if($usuarioOk === true){echo 'Mi perfil';}else{echo 'Iniciar sesión';};
              ?>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll" href="<?php 
            if($logeado === true){ echo '#'; }else{echo 'registro.php';}; ?>"><?php 
            if($logeado === true){ echo ''; }else{echo 'Registrarse';}; ?></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!--/ Nav End /-->

  <!--/ Intro Skew Star /-->
  <div id="home" class="intro route bg-image" style="background-color:white;">
    <div class="overlay-itro"></div>
    <div class="intro-content display-table">
      <div class="table-cell">
        <div class="container">
          <h1 class="intro-title mb-4"><img class="logoP" src="img/logos/logox2.png" alt=""></h1>
          <a href="registro.php" class="button button-a bg-dark button-middle button-rouded">Registro de talento</a>&nbsp;
          <a href="<?php 
            if($adminOk === true){ echo 'dashboard.php'; }else if($usuarioOk === true){echo 'mi-perfil.php';}else{echo 'login.php';}; ?>" class="button button-a bg-dark button-middle button-rouded">Ingresar a mi cuenta</a><br><br><br>
          <!--<p class="intro-subtitle"><span class="text-slider-items">Casting de teatro*, Nuevos proyectos*, Cine dramático*,  Dirección profesional*, </span><strong class="text-slider"></strong></p> -->
          <!-- <p class="pt-3"><a class="btn btn-primary btn js-scroll px-4" href="#about" role="button">Learn More</a></p> -->
        </div>
      </div>
    </div>
  </div>
  <!--/ Intro Skew End /-->

  

  <section id="nosotros" class="blog-mf sect-pt4 route" style="background-color:#fff;">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="title-box text-center">
            <h3 class="title-a">
              Acerca de nosotros
            </h3>
            <p class="subtitle-a">
              
            </p>
            <div class="line-mf"></div>
          </div>
        </div>
      </div>
      <div class="row" style="font-size:20px;color:#666;">
        <div class="col-md-12">
            <ul style="list-style:none;">
                <li>
                Tenemos mas 10 años de experiencia en las diferentes áreas de cine ,teatro y televisión.
Nos dimos a la tarea de formar un equipo que seleccionara talento con la capacidad y disposición para crear una atmósfera en cada escena.

                </li><br>
            </ul>
        </div>
      </div>
    </div><br><br><br>
  </section>

  <!--/ Section Blog Star /-->
  <section id="blog" class="blog-mf sect-pt4 route" style="background-color:#fff;">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="title-box text-center">
            <h3 class="title-a">
              Nuestro servicio
            </h3>
            <p class="subtitle-a">
              
            </p>
            <div class="line-mf"></div>
          </div>
        </div>
      </div>
      <div class="row" style="font-size:20px;color:#666;">
        <div class="col-md-12 text-center">
          <img src="img/logos/logox2.png" width="200px" alt=""><br><br>
          <div class="text-left"><h6>
            <p>En nuestra agencia queremos llevar a su proyecto personajes comprometidos a estar presentes .</p>
            <p>Contamos con una base de datos nacional. Nuestra página nos permite reclutar talento con diversas características especificas, look, genero, nacionalidad, etc... así como los que tienen entrenamiento o profesión específica que otorgan asesoría y/o participación totalmente realista a la escena.</p>
            <p>A todo nuestro talento se le asigna un código que facilita ,entre otras cosas , el control y secuencia de todos en caso de ser un grupo muy numeroso de talento.</p>
          
            <p>Si el cliente lo requiere, puede acceder a nuestro catálogo en línea y seleccionar personalmente al talento.</p>
          
            <p>Vivimos tiempos de cambio, nuestros coordinadores están capacitados en medidas sanitarias contra el COVID-19 y cuentan con antibacterial para proporcionar al talento antes y después de cada escena y/o cada hr.</p>
          
            <p>Contamos con transporte particular (de ser requerido adicionalmente).</p>
          
            <p>Lo hacemos personal. Una vez tengamos en las manos su proyecto, siempre tendrá una persona dedicada a sus necesidades de talento disponible 24-7 por cualquier eventualidad.</p>
          
            <p>Nuestro objetivo es proporcionar el talento que va a contribuir para crear escenas que provoquen una emoción en el espectador.</p>
            </h6>
          </div>
        </div>
        <div class="col-md-12 text-center mt-0"><br>
          <span><strong>
          <a class="nav-link js-scroll button button-a bg-dark button-middle button-rouded" href="#contact">* Contáctanos para más detalle sobre nuestro servicio</a>
          </strong></span><br><br><br>
          <h5><strong>“Tú lo buscas, nosotros lo encontramos”</strong> </h5>
          <br><br>
        </div>
      </div>
    </div>
  </section>
  <!--/ Section Blog End /-->



  <!--/ Section Contact-Footer Star /-->
  <section class="paralax-mf footer-paralax sect-mt4 route" style="background-color:white;">
    <!-- <div class="overlay-mf"></div> -->
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="contact-mf">
            <div id="contact" class="box-shadow-full">
              <div class="row">
                <div class="col-md-6">
                  <div class="title-box-2">
                    <h5 class="title-left">
                      Envía un mensaje
                    </h5>
                  </div>
                  <div>
                      <form action="contactform/contactform.php" method="post" role="form" class="">
                      <div id="sendmessage">Tu mensaje ha sido enviado, ¡Gracias!</div>
                      <div id="errormessage"></div>
                      <div class="row">
                        <div class="col-md-12 mb-3">
                          <div class="form-group">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Tu nombre" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres" />
                            <div class="validation"></div>
                          </div>
                        </div>
                        <div class="col-md-12 mb-3">
                          <div class="form-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Tu correo" data-rule="email" data-msg="Correo inválido" />
                            <div class="validation"></div>
                          </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div class="form-group">
                              <input type="text" class="form-control" name="subject" id="subject" placeholder="Asunto" data-rule="minlen:4" data-msg="Ingresa al menos 8 caracteres" />
                              <div class="validation"></div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                          <div class="form-group">
                            <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Escribe tu mensaje" placeholder="Mensaje"></textarea>
                            <div class="validation"></div>
                          </div>
                        </div>
                        <div class="col-md-12 mb-3">
                          <div class="form-group">
                          <div class="g-recaptcha" data-sitekey="6LdX2doaAAAAAIDXnaJLzgHBUKG4rDjOEIVt9aKC"></div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <button type="submit" class="button button-a button-big button-rouded">Enviar mensaje</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="title-box-2 pt-4 pt-md-0">
                  </div>
                  <div class="more-info">
                    <p class="lead">
                      Puedes ponerte en contacto con nosotros en los teléfonos de oficina
                    </p>
                    <ul class="list-ico">
                      <li><i class="fa fa-whatsapp"></i> +52 56 2725 4970 </li>
                      <li><span class="ion-email"></span> contacto@grupomedialab.com</li>
                    </ul>
                  </div>
                  <div class="socials">
                    <ul>
                      <!-- <li><a href="https://m.facebook.com/GRUPO-MEDIA-LAB-106382794882477/"><span class="ico-circle"><i class="ion-social-facebook"></i></span></a></li>
                      
                      <li><a href=""><span class="ico-circle"><i class="ion-social-twitter"></i></span></a></li>
                      <li><a href=""><span class="ico-circle"><i class="ion-social-pinterest"></i></span></a></li> -->
                      <li><a href="https://facebook.com/GRUPO-MEDIA-LAB-106382794882477/" target="blank"><span class="ico-circle"><i class="ion-social-facebook"></i></span></a></li>
                      <li><a href="https://www.instagram.com/grupomedialab/" target="blank"><span class="ico-circle"><i class="ion-social-instagram"></i></span></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="copyright-box">
              <p class="copyright" style="color:#000;">&copy; Copyright <strong>Grupo Media Lab</strong>. All Rights Reserved</p>
              <div class="credits">
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </section>
  <!--/ Section Contact-footer End /-->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader" align="center" class="justify-content-center">
    <img src="img/logos/logox2.png" style="margin-top:40%;" width="200px" height="140px" alt="">
  </div>
  
  
  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/popper/popper.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/counterup/jquery.waypoints.min.js"></script>
  <script src="lib/counterup/jquery.counterup.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <script src="lib/typed/typed.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
