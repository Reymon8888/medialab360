<?php

include('data/database.php');

$mensajeOk = '';
$mensajeError = '';
$ocultaBtnLogin = 'none';
$ocultaBtnRegistro = 'none';
if($_POST['contrasena'] === null or $_POST['contrasena'] === ''){
    header("location:registro.php");
    die();   
}
$correo = $_POST['correo'];
$tel1 = $_POST['tel1'];
$contrasena = $_POST['contrasena'];

$nombre = $_POST['nombre'];
$apellidoPat = $_POST['apellidoPat'];
$apellidoMat = $_POST['apellidoMat'];
$fechaNacimiento = $_POST['fechaNacimiento'];
$paisNacimiento = $_POST['paisNacimiento'];
$ubicacionActual = $_POST['ubicacionActual'];
$nombreContacto = $_POST['nombreContacto'];
$parentesco = $_POST['parentesco'];
$tel2 = $_POST['tel2'];

$genero = $_POST['genero'];
$edad = $_POST['edad'];
$estatura = $_POST['estatura'];
$peso = $_POST['peso'];
$complexion = $_POST['complexion'];
$aparienciaEtnica = $_POST['aparienciaEtnica'];
$colorPiel = $_POST['colorPiel'];
$colorOjos = $_POST['colorOjos'];
$colorCabello = $_POST['colorCabello'];
$largoCabello = $_POST['largoCabello'];
$estiloCabello = $_POST['estiloCabello'];
$tatuajes = $_POST['tatuajes'];
$piercings = $_POST['piercings'];

$danzas = $_POST['danzas'];
$deportes = $_POST['deportes'];
$instrumentos = $_POST['instrumentos'];
$desnudo = $_POST['desnudo'];
$idiomas = $_POST['idiomas'];
$descripcion = $_POST['descripcion'];

$valida_correo = mysqli_query($connection,"select * from usuarios where correo = '$correo' ");
$row = mysqli_fetch_array($valida_correo,MYSQLI_ASSOC);
$total_rows = mysqli_num_rows($valida_correo);

    if($total_rows != 0){
        $mensajeError = 'El correo que intentas registrar ya existe, intenta con uno distinto';
        $ocultaBtnRegistro = '';
    }else{

        $sql_query = "INSERT INTO usuarios (
            correo,
            telefono1, 
            contrasena, 
            nombre,
            apellido_pat,
            apellido_mat,
            fecha_nacimiento,
            pais_nacimiento,
            ubicacion_actual,
            nombre_contacto,
            parentesco,
            telefono2,
            genero,  
            edad,
            estatura,
            peso, 
            complexion, 
            apariencia_etnica,
            color_piel, 
            color_ojos, 
            color_cabello,
            largo_cabello,
            estilo_cabello, 
            tatuajes,
            piercings,
            danzas,
            deportes,
            instrumento,
            desnudo,
            idiomas, 
            descripcion, 
            tipo) 
        VALUES (
            '$correo',
            '$tel1', 
            '$contrasena', 
            '$nombre',
            '$apellidoPat',
            '$apellidoMat',
            '$fechaNacimiento',
            '$paisNacimiento',
            '$ubicacionActual',
            '$nombreContacto',
            '$parentesco',
            '$tel2',
            '$genero',  
            '$edad',
            '$estatura',
            '$peso', 
            '$complexion', 
            '$aparienciaEtnica',
            '$colorPiel', 
            '$colorOjos', 
            '$colorCabello',
            '$largoCabello',
            '$estiloCabello', 
            '$tatuajes',
            '$piercings',
            '$danzas',
            '$deportes',
            '$instrumentos',
            '$desnudo',
            '$idiomas', 
            '$descripcion',
            'Usuario'
        )";
    
    
        if ($connection->query($sql_query) === TRUE) {
            $mensajeOk = "<div class='text-success'>Te has registrado correctamente</div><br><strong class='text-success'>Ahora inicia sesión para completar tu perfil y cargar tus fotos</strong>";
            $subject = 'Confirmación de registro';
            $to = $correo;
            $subject2 = $subject;
            $message2 = "<h3>Gracias por registrarte!!!</h3>\r\nAhora puedes iniciar sesión en Grupo Media Lab con tu correo y contraseña<br>Usuario: <strong>".$correo."</strong><br>Contraseña: ".$contrasena."\r\n<a href='https://grupomedialab.com/login.php'><br><button>Iniciar sesión</button></a>";
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= "From: Grupo Media Lab<contacto@grupomedialab.com>\r\n" . "CCO: rarellano8888@gmail.com";
            
            mail($to, $subject2, $message2, $headers);
            $ocultaBtnLogin = '';
          } else {
            $mensajeError =  "Error durante el registro, intenta más tarde: <br>" . $connection->error;
          }

    }
    

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Grupo Media Lab</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
    
  <link rel="icon" href="img/logos/logox2.png" type="image/png" />
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300&family=Manrope:wght@200&display=swap" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: DevFolio
    Theme URL: https://bootstrapmade.com/devfolio-bootstrap-portfolio-html-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>
<style>
  body{
    cursor: url('img/cursor.png'), auto;
  }
</style>
<body id="page-top">

  <!--/ Nav Star /-->
  <nav class="navbar navbar-b navbar-trans navbar-expand-md fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll" href="#page-top"><img width="70px" src="img/logos/logox2.png" alt=""></a>
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link js-scroll active" href="index.php">Inicio</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll active" href="login.php">Ingresar</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!--/ Nav End /-->

  <!--/ Intro Skew Star /-->
  <div id="home" class="intro route bg-image" style="background-color:#fff;">
    <div class="overlay-itro"></div>
    <div class="intro-content display-table">
      <div class="table-cell">
        <div class="container"><br><br>
            <div class="row">
                <div class="col-sm-12">
                  <div class="contact-mf">
                    <div id="contact" class="box-shadow-full">
                      <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                          <div class="title-box-2">
                            <h5 class="title-left">
                              Registro
                            </h5><br>
                            <img src="img/logos/logox2.png" width="30%" alt="">
                          </div>
                          <div>
                              <?php echo $mensajeOk ?>
                              <div id="" class="text-danger"><?php echo $mensajeError ?></div>
                              <br>
                              <div class="row">
                                <div class="col-md-12">
                                <a href="registro.php" style="display:<?php echo $ocultaBtnRegistro;?>;" class="button button-a button-big button-rouded">Volver</a>
                                  <a href="login.php" style="display:<?php echo $ocultaBtnLogin;?>;" class="button button-a button-big button-rouded">Iniciar sesión</a>
                                </div>
                              </div>
                          </div>
                        </div>
                        <div class="col-md-2"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        </div>
      </div>
    </div>
  </div>
  <!--/ Intro Skew End /-->

  <!--/ Section Services Star /-->
  
  <!--/ Section Services End /-->

  <!--/ Section Portfolio Star /-->
  
  <!--/ Section Portfolio End /-->

  <!--/ Section Testimonials Star /-->

  <!--/ Section Blog Star /-->
 
  <!--/ Section Blog End /-->

  <!--/ Section Contact-Footer Star /-->
  <section class="paralax-mf footer-paralax bg-image sect-mt4 route" style="background-image: url(img/overlay-bg.jpg)">
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="copyright-box">
                <p class="copyright">&copy; Copyright <strong>Media Lab 360</strong>. All Rights Reserved</p>
              <div class="credits">
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </section>
  <!--/ Section Contact-footer End /-->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader" align="center" class="justify-content-center">
    <img src="img/logos/logox2.png" style="margin-top:40%;" width="200px" height="140px" alt="">
  </div>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/popper/popper.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/counterup/jquery.waypoints.min.js"></script>
  <script src="lib/counterup/jquery.counterup.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <script src="lib/typed/typed.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
