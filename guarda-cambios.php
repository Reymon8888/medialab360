<?php

include('data/database.php');
session_start();

$user_check = $_SESSION['usuario'];
$tipo = $_SESSION['tipo'];

$ses_sql = mysqli_query($connection,"select * from usuarios where correo = '$user_check' ");

$rowUs = mysqli_fetch_array($ses_sql,MYSQLI_ASSOC);
$idUsuario = $rowUs['id'];
$usuario = $rowUs['nombre'];
$correoUser = $rowUs['correo'];
$mensajeError = '';

$correo = $_POST['correo'];
$tel1 = $_POST['tel1'];

$nombre = $_POST['nombre'];
$apellidoPat = $_POST['apellidoPat'];
$apellidoMat = $_POST['apellidoMat'];
$fechaNacimiento = $_POST['fechaNacimiento'];
$paisNacimiento = $_POST['paisNacimiento'];
$ubicacionActual = $_POST['ubicacionActual'];
$nombreContacto = $_POST['nombreContacto'];
$parentesco = $_POST['parentesco'];
$tel2 = $_POST['tel2'];

$genero = $_POST['genero'];
$edad = $_POST['edad'];
$estatura = $_POST['estatura'];
$peso = $_POST['peso'];
$complexion = $_POST['complexion'];
$aparienciaEtnica = $_POST['aparienciaEtnica'];
$colorPiel = $_POST['colorPiel'];
$colorOjos = $_POST['colorOjos'];
$colorCabello = $_POST['colorCabello'];
$largoCabello = $_POST['largoCabello'];
$estiloCabello = $_POST['estiloCabello'];
$tatuajes = $_POST['tatuajes'];
$piercings = $_POST['piercings'];

$danzas = $_POST['danzas'];
$deportes = $_POST['deportes'];
$instrumentos = $_POST['instrumentos'];
$desnudo = $_POST['desnudo'];
$idiomas = $_POST['idiomas'];
$descripcion = $_POST['descripcion'];

$valida_correo = mysqli_query($connection,"select * from usuarios where correo = '$correo' ");
$row = mysqli_fetch_array($valida_correo,MYSQLI_ASSOC);
$total_rows = mysqli_num_rows($valida_correo);


if($correo != $correoUser){
    if($total_rows != 0){
        $mensajeError = 'El correo que intentas registrar ya existe, intenta con uno distinto';
    }else{

        $sql_query = "UPDATE usuarios SET 
            correo = '$correo',
            telefono1 = '$tel1', 
            nombre = '$nombre',
            apellido_pat = '$apellidoPat',
            apellido_mat = '$apellidoMat',
            fecha_nacimiento = '$fechaNacimiento',
            pais_nacimiento = '$paisNacimiento',
            ubicacion_actual = '$ubicacionActual',
            nombre_contacto = '$nombreContacto',
            parentesco = '$parentesco',
            telefono2 = '$tel2',
            genero = '$genero',  
            edad = '$edad',
            estatura = '$estatura',
            peso = '$peso', 
            complexion = '$complexion', 
            apariencia_etnica = '$aparienciaEtnica',
            color_piel = '$colorPiel', 
            color_ojos = '$colorOjos', 
            color_cabello = '$colorCabello',
            largo_cabello = '$largoCabello',
            estilo_cabello = '$estiloCabello', 
            tatuajes = '$tatuajes',
            piercings = '$piercings',
            danzas = '$danzas',
            deportes = '$deportes',
            instrumento = '$instrumentos',
            desnudo = '$desnudo',
            idiomas = '$idiomas', 
            descripcion = '$descripcion'
        WHERE id = '$idUsuario'";
        if ($connection->query($sql_query) === TRUE) {
        $mensajeError = "Cambios guardados correctamente <br>Como cambiaste de correo debes iniciar sesión nuevamente usando como usuario tu nuevo correo";
        } else {
        $mensajeError =  "Error durante el registro, intenta más tarde: " . $connection->error;
        }
        session_destroy();

    }
    
    
}else{

    $sql_query = "UPDATE usuarios SET 
            correo = '$correo',
            telefono1 = '$tel1', 
            nombre = '$nombre',
            apellido_pat = '$apellidoPat',
            apellido_mat = '$apellidoMat',
            fecha_nacimiento = '$fechaNacimiento',
            pais_nacimiento = '$paisNacimiento',
            ubicacion_actual = '$ubicacionActual',
            nombre_contacto = '$nombreContacto',
            parentesco = '$parentesco',
            telefono2 = '$tel2',
            genero = '$genero',  
            edad = '$edad',
            estatura = '$estatura',
            peso = '$peso', 
            complexion = '$complexion', 
            apariencia_etnica = '$aparienciaEtnica',
            color_piel = '$colorPiel', 
            color_ojos = '$colorOjos', 
            color_cabello = '$colorCabello',
            largo_cabello = '$largoCabello',
            estilo_cabello = '$estiloCabello', 
            tatuajes = '$tatuajes',
            piercings = '$piercings',
            danzas = '$danzas',
            deportes = '$deportes',
            instrumento = '$instrumentos',
            desnudo = '$desnudo',
            idiomas = '$idiomas', 
            descripcion = '$descripcion'
        WHERE id = '$idUsuario'";
        if ($connection->query($sql_query) === TRUE) {
            header("Location:mi-perfil.php");
        } else {
        $mensajeError =  "Error durante el registro, intenta más tarde: " . $connection->error;
        }

}

    

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?php echo $row['nombre']; ?></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
    
  <link rel="icon" href="img/logos/logox2.png" type="image/png" />
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300&family=Manrope:wght@200&display=swap" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: DevFolio
    Theme URL: https://bootstrapmade.com/devfolio-bootstrap-portfolio-html-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>
<style>
  body{
    cursor: url('img/cursor.png'), auto;
  }
</style>
<body id="page-top">

  <!--/ Nav Star /-->
  <nav class="navbar navbar-b navbar-trans navbar-expand-md fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll" href="#page-top"><img width="70px" src="img/logos/logox2.png" alt=""></a>
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link js-scroll active" href="index.php">Inicio</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link js-scroll" href="#work">Vitrina del cliente</a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link js-scroll" href="mi-perfil.php">Mi perfil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll" href="logout.php">Cerrar sesión</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!--/ Nav End /-->

  <!--/ Intro Skew Star /-->
  
<div class="overlay-itro"></div>
<section id="work" class="portfolio-mf sect-pt4 route" style="background-color:#fff;">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="title-box text-center">
            <h3 class="title-a"><br><br>
              <?php echo $rowUs['nombre']; ?>
            </h3>
            <div class="line-mf"></div><br>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
            <div class="alert alert-secondary" role="alert">
                <?php echo $mensajeError; ?> <a href="mi-perfil.php" class="btn bg-dark btn-sm text-light">Regresar</a>
            </div>
        </div>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
      </div>
    </div>
  </section>
    
  

  <!--/ Section Portfolio End /-->

  <!--/ Section Contact-footer End /-->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader" align="center" class="justify-content-center">
    <img src="img/logos/logox2.png" style="margin-top:40%;" width="200px" height="140px" alt="">
  </div>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/popper/popper.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/counterup/jquery.waypoints.min.js"></script>
  <script src="lib/counterup/jquery.counterup.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <script src="lib/typed/typed.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>