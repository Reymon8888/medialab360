<?php

include('data/database.php');
   session_start();

   $imgPred = '';
   
   $user_check = $_SESSION['usuario'];
   $tipo = $_SESSION['tipo'];
   
   $ses_sql = mysqli_query($connection,"select * from usuarios where correo = '$user_check' ");
   
   $row = mysqli_fetch_array($ses_sql,MYSQLI_ASSOC);
   $idUsuario = $row['id'];
   $usuario = $row['nombre'];
   
   $login_session = $row['correo'];
   
   if(!isset($_SESSION['usuario'])){
    header("location:login.php");
    die();   
   }else if($_SESSION['tipo'] !== 'Usuario'){
    header("location:login.php");
    die();  
   }

   $mensaje_imagenes = '';
   

    $query_imagenes = "SELECT * FROM imagenes WHERE usuario_id = '$idUsuario' AND estatus = 1 AND img_pred != 1 ORDER BY id ASC";
    $imagenes = mysqli_query($connection, $query_imagenes);
    $row_imagenes = mysqli_fetch_array($imagenes, MYSQLI_ASSOC);
    $totalRows_imagenes = mysqli_num_rows($imagenes);

   /* $pro = $_POST['producto'];
   $cat = $_POST['categoria'];
   $old = $_POST['old'];
   $oldImg = $_POST['img'];
   $img = $pro.'.jpg'; */

   /* if($pro !== $old){
       rename('<?php echo $fileDir; ?>'.$oldImg, '<?php echo $fileDir; ?>'.$img);
   } */
   $valida_img_pred ="SELECT * FROM imagenes WHERE usuario_id = '$idUsuario' AND img_pred = 1";
    $image_pred = mysqli_query($connection, $valida_img_pred);
    $imagPred = mysqli_fetch_array($image_pred, MYSQLI_ASSOC);
    $result_img_pred = mysqli_num_rows($image_pred);

    if($result_img_pred!= 0){
        $imgPred = $fileDir.$usuario.'/'.$imagPred['imagen'];
    }else{
        $imgPred = 'img/no-image-user.png';
    }

    if($result_img_pred === 0){
      $mensaje_imagenes = 'Aún no has agregado imágenes';
    }else if($totalRows_imagenes === 0){
      $mensaje_imagenes = 'Agrega más imágenes a tu perfil';
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?php echo $row['nombre'].' '.$row['apellido_pat'].' '.$row['apellido_mat']; ?></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
    
  <link rel="icon" href="img/logos/logox2.png" type="image/png" />
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300&family=Manrope:wght@200&display=swap" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: DevFolio
    Theme URL: https://bootstrapmade.com/devfolio-bootstrap-portfolio-html-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>
<script src="validaciones.js"></script>
<style>
  body{
    cursor: url('img/cursor.png'), auto;
  }
</style>
<body id="page-top">

  <!--/ Nav Star /-->
  <nav class="navbar navbar-b navbar-trans navbar-expand-md fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll" href="#page-top"><img width="70px" src="img/logos/logox2.png" alt=""></a>
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link js-scroll active" href="index.php">Inicio</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link js-scroll" href="#work">Vitrina del cliente</a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link js-scroll" href="mi-perfil.php">Mi perfil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll" href="logout.php">Cerrar sesión</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!--/ Nav End /-->

  <!--/ Intro Skew Star /-->
  
<div class="overlay-itro"></div>
<section id="work" class="portfolio-mf sect-pt4 route" style="background-color:#fff;">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="title-box text-center">
            <h3 class="title-a"><br><br>
             Editar información
            </h3>
            <div class="line-mf"></div>
          </div>
        </div>
      </div>
      <div  class="container">
            <div class="row">
                <div class="col-sm-12">
                  <div class="contact-mf">
                    <div id="contact" class="box-shadow-full">
                      <div class="row">
                        <div class="col-md-12">
                          <!-- <iframe src="stepper/stepper.php" frameborder="0" width="100%" height="600px"></iframe> -->
                          <div class="text-left" style="font-size:12px;">
                              <form action="guarda-cambios.php" onsubmit="return validar()" name="registro" method="POST" role="form" class="">
                              <div id="errormessage"></div>
                              <div id="accordion" class="p-2">
                                <!-- DATOS DE INICIO DE SESION -->
                                <div class="">
                                  <div class="" id="headingOne">
                                    <h5 class="mb-2">
                                      <a onClick="paso1()" id="paso1" style="cursor:pointer;color:rgb(90, 181, 10);" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        1. - Accesos &nbsp; <i class="fa fa-plus-circle"></i>&nbsp; <label id="alert1" style="display:none;color:red;font-size:14px;">*Tienes campos incompletos</label>
                                      </a><hr>
                                    </h5>
                                  </div>
                                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                      <div class="row">
                                        <div class="col-md-4">
                                          <div class="form-group">
                                          <label for="correo" class="text-dark">Correo</label>
                                            <input type="text" name="correo" value="<?php echo $row['correo']; ?>" class="form-control" id="correo" placeholder="Tu correo electrónico" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres" />
                                            <div class="validation"></div>
                                          </div>
                                        </div>   
                                        <div class="col-md-4">
                                          <div class="form-group">
                                          <label for="tel1" class="text-dark">Teléfono</label>
                                            <input type="text" name="tel1" value="<?php echo $row['telefono1']; ?>" class="form-control" id="tel1" placeholder="Tu teléfono" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres"  />
                                            <div class="validation"></div>
                                          </div>
                                        </div>                         
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!-- DATOS GENERALES -->
                                <div class="">
                                  <div class="" id="headingTwo">
                                    <h5 class="mb-2">
                                      <a onClick="paso2()" id="paso2" style="cursor:pointer;color:#999;" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        2. - Datos generales&nbsp; <i class="fa fa-plus-circle"></i>&nbsp; <label id="alert2" style="display:none;color:red;font-size:14px;">*Tienes campos incompletos</label>
                                      </a><hr>
                                    </h5>
                                  </div>
                                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                          <div class="form-group">
                                          <label for="nombre" class="text-dark">Nombre(s)</label>
                                            <input type="text" name="nombre"  value="<?php echo $row['nombre']; ?>" class="form-control" id="nombre" placeholder="Tu(s) nombre(s)" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres"  />
                                            <div class="validation"></div>
                                          </div>
                                        </div>   
                                        <div class="col-md-4">
                                          <div class="form-group">
                                          <label for="apellidoPat" class="text-dark">Apellido paterno</label>
                                            <input type="text" name="apellidoPat" value="<?php echo $row['apellido_pat']; ?>" class="form-control" id="apellidoPat" placeholder="Apellido paterno" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres"  />
                                            <div class="validation"></div>
                                          </div>
                                        </div>
                                        <div class="col-md-4">
                                          <div class="form-group">
                                          <label for="apellidoMat" class="text-dark">Apellido materno</label>
                                            <input type="text" name="apellidoMat" value="<?php echo $row['apellido_mat']; ?>" class="form-control" id="apellidoMat" placeholder="Apellido materno" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres"  />
                                            <div class="validation"></div>
                                          </div>
                                        </div>
                                        <div class="col-md-4">
                                          <div class="form-group">
                                          <label for="fechaNacimiento" class="text-dark">Fecha de nacimiento</label>
                                            <input type="date" name="fechaNacimiento" value="<?php echo $row['fecha_nacimiento']; ?>" class="form-control" id="fechaNacimiento" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres" />
                                            <div class="validation"></div>
                                          </div>
                                        </div>
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="nacionalidad" class="text-dark">Nacionalidad</label>
                                            <input type="text" name="paisNacimiento" value="<?php echo $row['pais_nacimiento']; ?>" id="paisNacimiento" class="form-control" id="nacionalidad" placeholder="Nacionalidad" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres"  />
                                            <div class="validation"></div>
                                          </div>
                                        </div>    
                                        <div class="col-md-5">
                                          <div class="form-group">
                                          <label for="ubicacionActual" class="text-dark">Ciudad actual</label>
                                            <input type="text" name="ubicacionActual" value="<?php echo $row['ubicacion_actual']; ?>" class="form-control" id="ubicacionActual" placeholder="Ciudad actual" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres"  />
                                            <div class="validation"></div>
                                          </div>
                                        </div>    
                                        <div class="col-md-5">
                                          <div class="form-group">
                                          <label for="nombreContacto" class="text-dark">¿A quién llamamos en caso de emergencia?</label>
                                            <input type="text" name="nombreContacto" value="<?php echo $row['nombre_contacto']; ?>" class="form-control" id="nombreContacto" placeholder="Nombre de contacto" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres"  />
                                            <div class="validation"></div>
                                          </div>
                                        </div>  
                                        <div class="col-md-4">
                                          <div class="form-group">
                                          <label for="parentesco" class="text-dark">Parentesco</label>
                                            <input type="text" name="parentesco" value="<?php echo $row['parentesco']; ?>" class="form-control" id="parentesco" placeholder="Parentesco" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres"  />
                                            <div class="validation"></div>
                                          </div>
                                        </div>    
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="tel2" class="text-dark">Teléfono de emergencias</label>
                                            <input type="text" name="tel2" value="<?php echo $row['telefono2']; ?>" class="form-control" id="tel2" placeholder="Número de emergencias" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres"  />
                                            <div class="validation"></div>
                                          </div>
                                        </div>          
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="">
                                  <div class="" id="headingThree">
                                    <h5 class="mb-0">
                                      <a onClick="paso3()" id="paso3" style="cursor:pointer;color:#999;" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                      3. - Aspecto físico&nbsp; <i class="fa fa-plus-circle"></i>&nbsp; <label id="alert3" style="display:none;color:red;font-size:14px;">*Tienes campos incompletos</label>
                                      </a><hr>
                                    </h5>
                                  </div>
                                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                      <div class="row">
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="genero" class="text-dark">Género</label>
                                            <select type="text" name="genero" class="form-control" id="genero"  >
                                              <option value="<?php echo $row['genero']; ?>"><?php echo $row['genero']; ?></option>
                                              <option value="">Género</option>
                                              <option value="Masculino">Masculino</option>
                                              <option value="Femenino">Femenino</option>
                                              <option value="Transgénero">Transgénero</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div>   
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="edad" class="text-dark">Edad</label>
                                          <input type="number" value="<?php echo $row['edad']; ?>" name="edad" class="form-control" id="edad" placeholder="Edad" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres"  />
                                            <div class="validation"></div>
                                          </div>
                                        </div>  
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="estatura" class="text-dark">Estatura (cm)</label>
                                          <input type="number" value="<?php echo $row['estatura']; ?>" name="estatura" class="form-control" id="estatura" placeholder="Estatura (cm)" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres"  />
                                            <div class="validation"></div>
                                          </div>
                                        </div>  
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="peso" class="text-dark">Peso (kg)</label>
                                          <input type="number" value="<?php echo $row['peso']; ?>" name="peso" class="form-control" id="peso" placeholder="Peso (kg)" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres"  />
                                            <div class="validation"></div>
                                          </div>
                                        </div>  
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="complexion" class="text-dark">Complexión</label>
                                            <select type="text" name="complexion" class="form-control" id="complexion" >
                                              <option value="<?php echo $row['complexion']; ?>"><?php echo $row['complexion']; ?></option>
                                              <option value="">Complexión</option>
                                              <option value="Delgada">Delgada</option>
                                              <option value="Atlética">Atlética</option>
                                              <option value="Pesada">Pesada</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div> 
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="aparienciaEtnica" class="text-dark">Aspecto étnco</label>
                                            <select type="text" name="aparienciaEtnica" class="form-control" id="aparienciaEtnica"  >
                                              <option value="<?php echo $row['apariencia_etnica']; ?>"><?php echo $row['apariencia_etnica']; ?></option>
                                              <option value="">Aspecto</option>
                                              <option value="Latino">Latino</option>
                                              <option value="Europeo">Europeo</option>
                                              <option value="Asiático">Asiático</option>
                                              <option value="Americano">Americano</option>
                                              <option value="Africano">Africano</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div>  
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="colorPiel" class="text-dark">Color de piel</label>
                                            <select type="text" name="colorPiel" class="form-control" id="colorPiel"  >
                                              <option value="<?php echo $row['color_piel']; ?>"><?php echo $row['color_piel']; ?></option>
                                              <option value="">Color de piel</option>
                                              <option value="Blanca">Blanca</option>
                                              <option value="Morena clara">Morena clara</option>
                                              <option value="Morena oscura">Morena oscura</option>
                                              <option value="Negra">Negra</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div> 
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="colorOjos" class="text-dark">Color de ojos</label>
                                            <select type="text" name="colorOjos" class="form-control" id="colorOjos"  >
                                              <option value="<?php echo $row['color_ojos']; ?>"><?php echo $row['color_ojos']; ?></option>
                                              <option value="">Color de ojos</option>
                                              <option value="Negros">Negros</option>
                                              <option value="Café oscuros">Café oscuros</option>
                                              <option value="Café claros">Café claros</option>
                                              <option value="Verde oscuros">Verdes oscuros</option>
                                              <option value="Verde claros">Verdes claros</option>
                                              <option value="Azul oscuros">Azul oscuros</option>
                                              <option value="Azul claros">Azul claros</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div>
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="colorCabello" class="text-dark">Color de cabello</label>
                                            <select type="text" name="colorCabello" class="form-control" id="colorCabello"  >
                                              <option value="<?php echo $row['color_cabello']; ?>"><?php echo $row['color_cabello']; ?></option>
                                              <option value="">Color de cabello</option>
                                              <option value="Negro">Negro</option>
                                              <option value="Castaño oscuro">Castaño oscuro</option>
                                              <option value="Castaño claro">Castaño claro</option>
                                              <option value="Rubio">Rubio</option>
                                              <option value="Pelirrojo">Pelirrojo</option>
                                              <option value="Semcano">Semicano</option>
                                              <option value="Cano">Cano</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div>
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="largoCabello" class="text-dark">Corte de cabello</label>
                                            <select type="text" name="largoCabello" class="form-control" id="largoCabello"  >
                                              <option value="<?php echo $row['largo_cabello']; ?>"><?php echo $row['largo_cabello']; ?></option>
                                              <option value="">Corte de cabello</option>
                                              <option value="Rasurado">Rasurado</option>
                                              <option value="Muy corto">Muy corto</option>
                                              <option value="Corto">Corto</option>
                                              <option value="A la barbilla">A la barbilla</option>
                                              <option value="Al hombro">Al hombro</option>
                                              <option value="Largo">Largo</option>
                                              <option value="Muy largo">Muy largo</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div>  
                                        <div class="col-md-2">
                                          <div class="form-group">
                                          <label for="estiloCabello" class="text-dark">Estilo de cabello</label>
                                            <select type="text" name="estiloCabello" class="form-control" id="estiloCabello"  >
                                              <option value="<?php echo $row['estilo_cabello']; ?>"><?php echo $row['estilo_cabello']; ?></option>
                                              <option value="">Estilo de cabello</option>
                                              <option value="Lacio">Lacio</option>
                                              <option value="Risado">Risado</option>
                                              <option value="Ondulado">Ondulado</option>
                                              <option value="Quebrado">Quebrado</option>
                                              <option value="Rastas">Rastas</option>
                                              <option value="Afro">Afro</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div> 
                                        <div class="col-md-2">
                                          <div class="form-group">
                                          <label for="tatuajes" class="text-dark">Tatuajes</label>
                                            <select type="text" name="tatuajes" class="form-control" id="tatuajes"  >
                                              <option value="<?php if($row['tatuajes']==''){echo '';}else{ echo $row['tatuajes'];}; ?>"><?php if($row['tatuajes']==""){echo "Ninguno";}else{ echo $row['tatuajes'];}; ?></option>
                                              <option value="">Ninguno</option>
                                              <option value="Brazos">Brazos</option>
                                              <option value="Piernas">Piernas</option>
                                              <option value="Rostro">Rostro</option>
                                              <option value="Cuello">Cuello</option>
                                              <option value="Espalda">Espalda</option>
                                              <option value="Manos">Manos</option>
                                              <option value="Pies">Pies</option>
                                              <option value="Multiples">Multiples</option>
                                              <option value="Cuerpo entero">Cuerpo entero</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div> 
                                        <div class="col-md-2">
                                          <div class="form-group">
                                          <label for="piercings" class="text-dark">Perforaciones</label>
                                            <select type="text" name="piercings" class="form-control" id="piercings"  >
                                              <option value="<?php if($row['piercings']==''){echo '';}else{ echo $row['piercings'];}; ?>"><?php if($row['piercings']==""){echo "Ninguno";}else{ echo $row['piercings'];}; ?></option>
                                              <option value="">Ninguno</option>
                                              <option value="Oídos">Oídos</option>
                                              <option value="Rostro">Rostro</option>
                                              <option value="Vientre">Vientre</option>
                                              <option value="Pezones">Pezones</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div>                        
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="">
                                  <div class="" id="headingThree">
                                    <h5 class="mb-0">
                                      <a onClick="paso4()" id="paso4" style="cursor:pointer;color:#999;" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                      4. - Habilidades y talentos&nbsp; <i class="fa fa-plus-circle"></i>&nbsp; <label id="alert4" style="display:none;color:red;font-size:14px;">*Tienes campos incompletos</label>
                                      </a><hr>
                                    </h5>
                                  </div>
                                  <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                    <div class="card-body">
                                      <div class="row">
                                      <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="danzas" class="text-dark">¿Bailas?</label>
                                            <textarea name="danzas" id="" cols="30" class="form-control" rows="10" placeholder="Enlista los estilos que dominas"><?php echo $row['danzas']; ?></textarea>
                                            <div class="validation"></div>
                                          </div>
                                        </div>    
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="deportes" class="text-dark">¿Practicas deportes?</label>
                                          <textarea name="deportes" id="" cols="30" class="form-control" rows="10" placeholder="Enlista los deportes que practicas"><?php echo $row['deportes']; ?></textarea>
                                            <div class="validation"></div>
                                          </div>
                                        </div>
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="instrumentos" class="text-dark">¿Tocas algún instrumento?</label>
                                          <textarea name="instrumentos" id="" cols="30" class="form-control" rows="10" placeholder="Enlista los instrumentos musicales que dominas"><?php echo $row['instrumento']; ?></textarea>
                                            <div class="validation"></div>
                                          </div>
                                        </div>     
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="desnudo" class="text-dark">¿Realizas desnudos?</label>
                                          <div class="form-check">
                                              <input class="form-check-input" type="radio" value="1" name="desnudo" id="flexRadioDefault1" <?php if($row['desnudo']==1){echo 'checked';} ?>>
                                              <label class="form-check-label text-dark" for="flexRadioDefault1">
                                                Si
                                              </label>
                                            </div>
                                            <div class="form-check">
                                              <input class="form-check-input" type="radio" vaue="0" name="desnudo" id="flexRadioDefault2" <?php if($row['desnudo']==0){echo 'checked';} ?>>
                                              <label class="form-check-label text-dark" for="flexRadioDefault2">
                                                No
                                              </label>
                                            </div>
                                            <div class="validation"></div>
                                          </div>
                                        </div> 
                                        <div class="col-md-6">
                                          <div class="form-group">
                                          <label for="idiomas" class="text-dark">Idiomas</label>
                                            <input type="text" name="idiomas" value="<?php echo $row['idiomas']; ?>" class="form-control" id="idiomas" placeholder="Idiomas que dominas" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres"  />
                                            <div class="validation"></div>
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                          <label for="idiomas" class="text-dark">Información adicional</label>
                                            <textarea type="text" name="descripcion" class="form-control" id="descripcion" placeholder="Háblanos más de ti" data-rule="minlen:4" data-msg="Ingresa al menos 4 caracteres" rows="3" ><?php echo $row['descripcion']; ?></textarea>
                                            <div class="validation"></div>
                                          </div>
                                        </div>                       
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <button type="submit" class="btn btn-dark">Guardar cambios</button>&nbsp;&nbsp;
                                  <a href="mi-perfil.php" class="btn btn-dark">Cancelar</a>&nbsp;&nbsp;
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        </div>
    </div><br><br><br><br>
  </section>
    
  

  <!--/ Section Portfolio End /-->

  <!--/ Section Contact-footer End /-->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader" align="center" class="justify-content-center">
    <img src="img/logos/logox2.png" style="margin-top:40%;" width="200px" height="140px" alt="">
  </div>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/popper/popper.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/counterup/jquery.waypoints.min.js"></script>
  <script src="lib/counterup/jquery.counterup.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <script src="lib/typed/typed.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
