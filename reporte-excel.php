<?php

include('data/database.php');

$consulta = "SELECT * FROM usuarios WHERE tipo = 'Usuario' ORDER BY id ASC ";
  
$resultado = $connection->query($consulta);

if($resultado->num_rows > 0 ){

    date_default_timezone_set('America/Mexico_City');
    if (PHP_SAPI == 'cli')
    die('Este archivo solo se puede ver desde un navegador web');
    /** Se agrega la libreria PHPExcel */
    require_once 'Classes/PHPExcel.php';
    
    // Se crea el objeto PHPExcel
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->getProperties()->setCreator("Grupo Media Lab") // Nombre del autor
    ->setLastModifiedBy("Grupo Media Lab") //Ultimo usuario que lo modificó
    ->setTitle("Reporte Excel") // Titulo
    ->setSubject("Reporte Excel") //Asunto
    ->setDescription("Reporte de talento") //Descripción
    ->setKeywords("reporte usuarios registrados") //Etiquetas
    ->setCategory("Reporte excel"); //Categ

    $tituloReporte = "Base de datos de usuarios";
    $titulosColumnas = array('ID', 'EMAIL', 'TELEFONO', 'NOMBRE', 'APELLIDO_PAT', 'APELLIDO_MAT', 'FECHA_NACIM', 'NACIONALIDAD', 'CIUDAD_ACTUAL', 'CONTACTO', 'TEL_CONTACTO', 'PARENT', 'GENERO', 'EDAD', 'ASPECT_ET', 'COLOR_PIEL', 'COLOR_OJOS', 'COLOR_CABELLO', 'CORTE', 'ESTILO_CABELLO', 'TATUAJES', 'PERFORACIONES', 'EST_BAILE', 'DEPORTES', 'INSTRUMENTOS', 'DESNUDO', 'IDIOMAS', 'INFO_ADCIONAL');

    $objPHPExcel->setActiveSheetIndex(0)
    ->mergeCells('A1:AB1');
  
// Se agregan los titulos del reporte
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1',$tituloReporte) // Titulo del reporte
        ->setCellValue('A3',  $titulosColumnas[0])  //Titulo de las columnas
        ->setCellValue('B3',  $titulosColumnas[1])
        ->setCellValue('C3',  $titulosColumnas[2])
        ->setCellValue('D3',  $titulosColumnas[3])
        ->setCellValue('E3',  $titulosColumnas[4])
        ->setCellValue('F3',  $titulosColumnas[5])
        ->setCellValue('G3',  $titulosColumnas[6])
        ->setCellValue('H3',  $titulosColumnas[7])
        ->setCellValue('I3',  $titulosColumnas[8])
        ->setCellValue('J3',  $titulosColumnas[9])
        ->setCellValue('K3',  $titulosColumnas[10])
        ->setCellValue('L3',  $titulosColumnas[11])
        ->setCellValue('M3',  $titulosColumnas[12])
        ->setCellValue('N3',  $titulosColumnas[13])
        ->setCellValue('O3',  $titulosColumnas[14])
        ->setCellValue('P3',  $titulosColumnas[15])
        ->setCellValue('Q3',  $titulosColumnas[16])
        ->setCellValue('R3',  $titulosColumnas[17])
        ->setCellValue('S3',  $titulosColumnas[18])
        ->setCellValue('T3',  $titulosColumnas[19])
        ->setCellValue('U3',  $titulosColumnas[20])
        ->setCellValue('V3',  $titulosColumnas[21])
        ->setCellValue('W3',  $titulosColumnas[22])
        ->setCellValue('X3',  $titulosColumnas[23])
        ->setCellValue('Y3',  $titulosColumnas[24])
        ->setCellValue('Z3',  $titulosColumnas[25])
        ->setCellValue('AA3',  $titulosColumnas[26])
        ->setCellValue('AB3',  $titulosColumnas[27]);

        $i = 4; //Numero de fila donde se va a comenzar a rellenar
        while ($fila = $resultado->fetch_array()) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $fila['id'])
                ->setCellValue('B'.$i, $fila['correo'])
                ->setCellValue('C'.$i, $fila['telefono1'])
                ->setCellValue('D'.$i, $fila['nombre'])
                ->setCellValue('E'.$i, $fila['apellido_pat'])
                ->setCellValue('F'.$i, $fila['apellido_mat'])
                ->setCellValue('G'.$i, $fila['fecha_nacimiento'])
                ->setCellValue('H'.$i, $fila['pais_nacimiento'])
                ->setCellValue('I'.$i, $fila['ubicacion_actual'])
                ->setCellValue('J'.$i, $fila['nombre_contacto'])
                ->setCellValue('K'.$i, $fila['telefono2'])
                ->setCellValue('L'.$i, $fila['parentesco'])
                ->setCellValue('M'.$i, $fila['genero'])
                ->setCellValue('N'.$i, $fila['edad'])
                ->setCellValue('O'.$i, $fila['apariencia_etnica'])
                ->setCellValue('P'.$i, $fila['color_piel'])
                ->setCellValue('Q'.$i, $fila['color_ojos'])
                ->setCellValue('R'.$i, $fila['color_cabello'])
                ->setCellValue('S'.$i, $fila['largo_cabello'])
                ->setCellValue('T'.$i, $fila['estilo_cabello'])
                ->setCellValue('U'.$i, $fila['tatuajes'])
                ->setCellValue('V'.$i, $fila['piercings'])
                ->setCellValue('W'.$i, $fila['danzas'])
                ->setCellValue('X'.$i, $fila['deportes'])
                ->setCellValue('Y'.$i, $fila['instrumento'])
                ->setCellValue('Z'.$i, $fila['desnudo'])
                ->setCellValue('AA'.$i, $fila['idiomas'])
                ->setCellValue('AB'.$i, $fila['descripcion']);
            $i++;
        }

        $estiloTituloReporte = array(
            'font' => array(
                'name'      => 'Verdana',
                'bold'      => true,
                'italic'    => false,
                'strike'    => false,
                'size' =>16,
                'color'     => array(
                    'rgb' => 'FFFFFF'
                )
            ),
            'fill' => array(
              'type'  => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array(
                    'rgb' => '000000')
          ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_NONE
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
                'wrap' => TRUE
            )
        );
          
        $estiloTituloColumnas = array(
            'font' => array(
                'name'  => 'Arial',
                'bold'  => true,
                'color' => array(
                    'rgb' => 'FFFFFF'
                )
            ),
            'fill' => array(
                'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
          'rotation'   => 90,
                'startcolor' => array(
                    'rgb' => '333333'
                ),
                'endcolor' => array(
                    'rgb' => '999999'
                )
            ),
            'borders' => array(
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                    'color' => array(
                        'rgb' => '000000'
                    )
                ),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                    'color' => array(
                        'rgb' => 'FFFFFF'
                    )
                )
            ),
            'alignment' =>  array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap'      => TRUE
            )
        );
          
        $estiloInformacion = new PHPExcel_Style();
        $estiloInformacion->applyFromArray( array(
            'font' => array(
                'name'  => 'Arial',
                'color' => array(
                    'rgb' => '000000'
                )
            ),
            'fill' => array(
          'type'  => PHPExcel_Style_Fill::FILL_SOLID,
          'color' => array(
                    'rgb' => 'FFFFFF')
          ),
            'borders' => array(
                'left' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN ,
              'color' => array(
                      'rgb' => '3a2a47'
                    )
                )
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A1:AB1')->applyFromArray($estiloTituloReporte);
        $objPHPExcel->getActiveSheet()->getStyle('A3:AB3')->applyFromArray($estiloTituloColumnas);
        $objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:AB".($i-1));

        for($i = 'A'; $i <= 'AB'; $i++){
            $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($i)->setAutoSize(TRUE);
        }

        // Se asigna el nombre a la hoja
        $objPHPExcel->getActiveSheet()->setTitle('Usuarios');
        
        // Se activa la hoja para que sea la que se muestre cuando el archivo se abre
        $objPHPExcel->setActiveSheetIndex(0);
        
        // Inmovilizar paneles
        //$objPHPExcel->getActiveSheet(0)->freezePane('A4');
        $objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Reportedeusuarios.xlsx"');
        header('Cache-Control: max-age=0');
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
        }
        else{
            print_r('No hay resultados para mostrar');
        }
?>