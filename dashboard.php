<?php

include('data/database.php');
   session_start();
   
  $user_check = $_SESSION['usuario'];
  $tipo = $_SESSION['tipo'];

  $ses_sql = mysqli_query($connection,"select * from usuarios where correo = '$user_check' ");

  $row = mysqli_fetch_array($ses_sql,MYSQLI_ASSOC);

  $login_session = $row['correo'];

  if(!isset($_SESSION['usuario'])){
  header("location:login.php");
  die();   
  }else if($_SESSION['tipo'] !== 'Administrador'){
  header("location:login.php");
  die();  
  }
  
  

  if(!isset($_POST['busqueda'])){
    $query_actores = "SELECT * FROM usuarios WHERE tipo = 'Usuario' ORDER BY id DESC LIMIT 12";
    $actores = mysqli_query($connection, $query_actores);
    $row_actores = mysqli_fetch_array($actores, MYSQLI_ASSOC);
    $totalRows_actores = mysqli_num_rows($actores);
    if($totalRows_actores == 0){
      $mensaje = 'Aún no hay actores registrados';
    }else{
      $mensaje = 'Actores registrados recientemente';
    }
  }else{
  
  $criterio = $_POST['busqueda'];
  $query_actores = "SELECT *, concat_ws( ' ', nombre, apellido_pat, apellido_mat) AS nombre_completo FROM usuarios WHERE tipo = 'Usuario' AND concat_ws( ' ', nombre, apellido_pat, apellido_mat) LIKE '%$criterio%' ORDER BY id DESC";
  $actores = mysqli_query($connection, $query_actores);
  $row_actores = mysqli_fetch_array($actores, MYSQLI_ASSOC);
  $totalRows_actores = mysqli_num_rows($actores);
  if($totalRows_actores == 0){
    $mensaje = 'Sin resultados en la búsqueda &nbsp;<a href="dashboard.php" class="btn btn-sm btn-dark">Regresar</a><br><br><br><br><br><br>';
    }else if($totalRows_actores == 1){
      $mensaje = $totalRows_actores.' resultado encontrado &nbsp;<a href="dashboard.php" class="btn btn-sm btn-dark">Regresar</a>';
    }else{
      $mensaje = $totalRows_actores.' resultados encontrados &nbsp;<a href="dashboard.php" class="btn btn-sm btn-dark">Regresar</a>';
    }
  }

  $suma_query = "SELECT SUM(size) as total FROM imagenes";
  $espacio_utilizado = mysqli_query($connection, $suma_query);
  $esp_util = mysqli_fetch_array($espacio_utilizado, MYSQLI_ASSOC);
 
  $esp_en_mb = $esp_util['total'] / 1000000;
  $esp_corto_mb = number_format($esp_en_mb, 2);
  $espacio_asignado = 10000;
  $espacio_disponible = ($espacio_asignado) - ($esp_corto_mb);
  $porcentaje_uso = number_format($esp_corto_mb, 0);
  $porcent = $porcentaje_uso / 100;
  // pagination


      
  $limitpage = 2;
  $page = 1;
  if(isset($_GET["page"]) && $_GET["page"]!=""){ $page=$_GET["page"]; }
  $startpage = 0;
  $endpage = $limitpage;
  if($page > 1){  
    $resta = $page-1;
    $startpage = $resta * $limitpage;
    $endpage=($page)*$limitpage; 
  }
  //echo $startpage;
  $user_id=null;
  $sql0= "select count(*) as c from usuarios where tipo = 'Usuario'";
  $query0 = $connection->query($sql0);
  $count = $query0->fetch_array();
  $npages = $count["c"]/$limitpage;
  
  
  $sql1= "select * from usuarios where tipo = 'Usuario' limit $startpage,$limitpage";
  $query = $connection->query($sql1);

  // endpagination

  // exportar datos a excel

  /* $query_excel = "SELECT * FROM usuarios WHERE tipo = 'Usuario' ORDER BY id ASC";
  $data_excel = mysqli_query($connection, $query_excel);
  //$row_data = mysqli_fetch_array($data_excel, MYSQLI_ASSOC);
  $info = array();
  while($actores = mysqli_fetch_assoc($data_excel)){
    $todos[] = $actores; 
  }
  $totalRows_excel = mysqli_num_rows($data_excel);

  if(isset($_POST["export_data"])){
    if(!empty($todos)){
      $timestamp = time();
            $filename = 'Export_' . $timestamp . '.xls';
            
            header("Content-Type: application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=\"$filename\"");

      $mostrar_columnas = false;

      foreach($todos as $actor){
        if(!$mostrar_columnas){
          echo implode("\t", array_keys($actor))."\n";
          $mostrar_columnas = true;
        }

        echo implode("\t", array_values($actor))."\n";
      }
    }else{
      echo "No hay datos para exportar";
    }
    exit;
  }
*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Grupo Media Lab</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
    
  <link rel="icon" href="img/logos/logox2.png" type="image/png" />
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300&family=Manrope:wght@200&display=swap" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">


  <!-- =======================================================
    Theme Name: DevFolio
    Theme URL: https://bootstrapmade.com/devfolio-bootstrap-portfolio-html-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
  
</head>
<script src="validaciones.js"></script>
<style>
  body{
    cursor: url('img/cursor.png'), auto;
  }
</style>
<body id="page-top">

  <!--/ Nav Star /-->
  <nav class="navbar navbar-b navbar-trans navbar-expand-md fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll" href="#page-top"><img width="70px" src="img/logos/logox2.png" alt=""></a>
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link js-scroll active" href="index.php">Inicio</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link js-scroll" href="#work">Vitrina del cliente</a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link js-scroll" href="logout.php">Cerrar sesión</a>
          </li>
        </ul>
        <div class="">
          <form action="dashboard.php" method="POST">
            <div class="input-group">
              <input type="text" class="form-control" name="busqueda" id="busqueda" placeholder="Buscar por nombre..." data-rule="email" data-msg="Ingresa al menos 4 caracteres" required/>
              <span class="input-group-btn">
                <button class="btn btn-default btn-dark" type="submit">Buscar</button>
              </span>
              <br>
              <div class="validation"></div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </nav>
  <!--/ Nav End /-->

  <!--/ Intro Skew Star /-->
  
<div class="overlay-itro"></div>
<section id="work" class="portfolio-mf sect-pt4 route" style="background-color:#fff;">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="title-box text-center">
            <h3 class="title-a"><br><br>
            Grupo Media Lab
            </h3>
            <div class="line-mf"></div>
          </div>
        </div>
      </div>


     <!-- XXXXXXXX
     XXXXXXXXXX -->

     <div class="row">
                <div class="col-sm-12">
                  <div class="contact-mf">
                    <div id="contact" class="box-shadow-full">
                      <div class="row">
                        <div class="col-md-12">
                        <label for="">Espacio utilizado: <?php echo $esp_corto_mb.' MB ( '.$porcent.'% )'; ?> </label>&nbsp;&nbsp;<i class="fa fa-server"></i> 
                          <div class="progress">
                            <div class="progress-bar bg-dark progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $porcent; ?>%"></div>
                          </div><br>
                        </div>
                        <div class="col-md-12">
                          <form action="reporte-excel.php" method="POST">
                          <button type="submit" id="export_data" name="export_data" class="btn btn-dark">Descargar base de datos &nbsp;&nbsp;<i class="fa fa-download"></i></button><br><br>
                        </form>
                        </div>
                        <div class="col-md-12">
                          <!-- <iframe src="stepper/stepper.php" frameborder="0" width="100%" height="600px"></iframe> -->
                          <div class="text-left" style="font-size:12px;">
                              <form action="result.php" onsubmit="return validar()" name="registro" method="POST" role="form" class="">
                              <div id="errormessage"></div>
                              <div id="accordion" class="p-2">
                                <!-- Búsqueda avanzada -->
                                <div class="">
                                  <div class="" id="headingThree">
                                    <h5 class="mb-0">
                                      <a onClick="changeColor()" id="search" style="cursor:pointer;color:#999;" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                      Búsqueda avanzada de actores&nbsp; <i class="fa fa-search"></i>
                                      </a><hr>
                                    </h5>
                                  </div>
                                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                      <div class="row">
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="genero" class="text-dark">Género</label>
                                            <select type="text" name="genero" class="form-control" id="genero"  >
                                              <option value="">Cualquiera</option>
                                              <option value="Masculino">Masculino</option>
                                              <option value="Femenino">Femenino</option>
                                              <option value="Transgénero">Transgénero</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div>   
                                        <div class="col-md-3">
                                        <div class="form-group">
                                          <label for="edad" class="text-dark">Edad</label>
                                            <select type="text" name="edad" class="form-control" id="edad" >
                                              <option value="">Cualquiera</option>
                                              <option value="1">0 - 5 años</option>
                                              <option value="2">6 - 11 años</option>
                                              <option value="3">12 -17 años</option>
                                              <option value="4">18 - 26 años</option>
                                              <option value="5">27 - 59 años</option>
                                              <option value="6">60 años en adelante</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div>  
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="estatura" class="text-dark">Estatura</label>
                                            <select type="text" name="estatura" class="form-control" id="estatura" >
                                              <option value="">Cualquiera</option>
                                              <option value="1">Menos de 50 cm</option>
                                              <option value="2">51 - 100 cm</option>
                                              <option value="3">101 - 120 cm</option>
                                              <option value="4">121 - 150 cm</option>
                                              <option value="5">151 - 170 cm</option>
                                              <option value="6">171 - 190 cm</option>
                                              <option value="7">Más de 190 cm</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div>  
                                        <div class="col-md-3">
                                          <div class="form-group">
                                            <label for="peso" class="text-dark">Peso</label>
                                            <select type="text" name="peso" class="form-control" id="peso" >
                                              <option value="">Cualquiera</option>
                                              <option value="1">Menos de 20 Kg</option>
                                              <option value="2">21 - 50 Kg</option>
                                              <option value="3">51 - 60 Kg</option>
                                              <option value="4">61 - 70 Kg</option>
                                              <option value="5">71 - 80 Kg</option>
                                              <option value="6">81 - 90 Kg</option>
                                              <option value="7">Más de 90 Kg</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div>  
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="complexion" class="text-dark">Complexión</label>
                                            <select type="text" name="complexion" class="form-control" id="complexion" >
                                              <option value="">Cualquiera</option>
                                              <option value="Delgada">Delgada</option>
                                              <option value="Atlética">Atlética</option>
                                              <option value="Pesada">Pesada</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div> 
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="aparienciaEtnica" class="text-dark">Aspecto étnco</label>
                                            <select type="text" name="aparienciaEtnica" class="form-control" id="aparienciaEtnica"  >
                                              <option value="">Cualquiera</option>
                                              <option value="Latino">Latino</option>
                                              <option value="Europeo">Europeo</option>
                                              <option value="Asiático">Asiático</option>
                                              <option value="Americano">Americano</option>
                                              <option value="Africano">Africano</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div>  
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="colorPiel" class="text-dark">Color de piel</label>
                                            <select type="text" name="colorPiel" class="form-control" id="colorPiel"  >
                                              <option value="">Cualquiera</option>
                                              <option value="Blanca">Blanca</option>
                                              <option value="Morena clara">Morena clara</option>
                                              <option value="Morena oscura">Morena oscura</option>
                                              <option value="Negra">Negra</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div> 
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="colorOjos" class="text-dark">Color de ojos</label>
                                            <select type="text" name="colorOjos" class="form-control" id="colorOjos"  >
                                              <option value="">Cualquiera</option>
                                              <option value="Negros">Negros</option>
                                              <option value="Café oscuros">Café oscuros</option>
                                              <option value="Café claros">Café claros</option>
                                              <option value="Verde oscuros">Verdes oscuros</option>
                                              <option value="Verde claros">Verdes claros</option>
                                              <option value="Azul oscuros">Azul oscuros</option>
                                              <option value="Azul claros">Azul claros</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div>
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="colorCabello" class="text-dark">Color de cabello</label>
                                            <select type="text" name="colorCabello" class="form-control" id="colorCabello"  >
                                              <option value="">Cualquiera</option>
                                              <option value="Negro">Negro</option>
                                              <option value="Castaño oscuro">Castaño oscuro</option>
                                              <option value="Castaño claro">Castaño claro</option>
                                              <option value="Rubio">Rubio</option>
                                              <option value="Pelirrojo">Pelirrojo</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div>
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="largoCabello" class="text-dark">Corte de cabello</label>
                                            <select type="text" name="largoCabello" class="form-control" id="largoCabello"  >
                                              <option value="">Cualquiera</option>
                                              <option value="Rasurado">Rasurado</option>
                                              <option value="Muy corto">Muy corto</option>
                                              <option value="Corto">Corto</option>
                                              <option value="A la barbilla">A la barbilla</option>
                                              <option value="Al hombro">Al hombro</option>
                                              <option value="Largo">Largo</option>
                                              <option value="Muy largo">Muy largo</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div>  
                                        <div class="col-md-2">
                                          <div class="form-group">
                                          <label for="estiloCabello" class="text-dark">Estilo de cabello</label>
                                            <select type="text" name="estiloCabello" class="form-control" id="estiloCabello"  >
                                              <option value="">Cualquiera</option>
                                              <option value="Lacio">Lacio</option>
                                              <option value="Risado">Risado</option>
                                              <option value="Ondulado">Ondulado</option>
                                              <option value="Quebrado">Quebrado</option>
                                              <option value="Rastas">Rastas</option>
                                              <option value="Afro">Afro</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div> 
                                        <div class="col-md-2">
                                          <div class="form-group">
                                          <label for="tatuajes" class="text-dark">Tatuajes</label>
                                            <select type="text" name="tatuajes" class="form-control" id="tatuajes"  >
                                              <option value="Indistinto">Indistinto</option>
                                              <option value="">Ninguno</option>
                                              <option value="Brazos">Brazos</option>
                                              <option value="Piernas">Piernas</option>
                                              <option value="Rostro">Rostro</option>
                                              <option value="Cuello">Cuello</option>
                                              <option value="Espalda">Espalda</option>
                                              <option value="Manos">Manos</option>
                                              <option value="Pies">Pies</option>
                                              <option value="Multiples">Multiples</option>
                                              <option value="Cuerpo entero">Cuerpo entero</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div> 
                                        <div class="col-md-2">
                                          <div class="form-group">
                                          <label for="piercings" class="text-dark">Perforaciones</label>
                                            <select type="text" name="piercings" class="form-control" id="piercings"  >
                                              <option value="Indistinto">Indistinto</option>
                                              <option value="">Ninguno</option>
                                              <option value="Oídos">Oídos</option>
                                              <option value="Rostro">Rostro</option>
                                              <option value="Vientre">Vientre</option>
                                              <option value="Pezones">Pezones</option>
                                            </select>
                                            <div class="validation"></div>
                                          </div>
                                        </div>
                                        <div class="col-md-3">
                                          <div class="form-group">
                                            <label for="danzas" class="text-dark">¿El actor baila?</label>
                                            <div class="form-check">
                                              <input class="form-check-input" type="radio" value="Si" name="danzas" id="flexRadioDefault1">
                                              <label class="form-check-label text-dark" for="flexRadioDefault1">
                                                Si
                                              </label>
                                            </div>
                                            <div class="form-check">
                                              <input class="form-check-input" type="radio" value="" name="danzas" id="flexRadioDefault2" checked>
                                              <label class="form-check-label text-dark" for="flexRadioDefault2">
                                                No importa
                                              </label>
                                            </div>
                                            <div class="validation"></div>
                                          </div>
                                        </div>    
                                        <div class="col-md-3">
                                          <div class="form-group">
                                            <label for="deportes" class="text-dark">¿El actor practica deportes?</label>
                                            <div class="form-check">
                                              <input class="form-check-input" type="radio" value="Si" name="deportes" id="flexRadioDefault1">
                                              <label class="form-check-label text-dark" for="flexRadioDefault1">
                                                Si
                                              </label>
                                            </div>
                                            <div class="form-check">
                                              <input class="form-check-input" type="radio" value="" name="deportes" id="flexRadioDefault2" checked>
                                              <label class="form-check-label text-dark" for="flexRadioDefault2">
                                                No importa
                                              </label>
                                            </div>
                                            <div class="validation"></div>
                                          </div>
                                        </div> 
                                        <div class="col-md-3">
                                          <div class="form-group">
                                            <label for="instrumentos" class="text-dark">¿El actor domina algún instrumento?</label>
                                            <div class="form-check">
                                              <input class="form-check-input" type="radio" value="Si" name="instrumentos" id="flexRadioDefault1">
                                              <label class="form-check-label text-dark" for="flexRadioDefault1">
                                                Si
                                              </label>
                                            </div>
                                            <div class="form-check">
                                              <input class="form-check-input" type="radio" value="" name="instrumentos" id="flexRadioDefault2" checked>
                                              <label class="form-check-label text-dark" for="flexRadioDefault2">
                                                No importa
                                              </label>
                                            </div>
                                            <div class="validation"></div>
                                          </div>
                                        </div>   
                                        <div class="col-md-3">
                                          <div class="form-group">
                                          <label for="desnudo" class="text-dark">¿Realizas desnudos?</label>
                                          <div class="form-check">
                                              <input class="form-check-input" type="radio" value="Si" name="desnudo" id="flexRadioDefault1">
                                              <label class="form-check-label text-dark" for="flexRadioDefault1">
                                                Si
                                              </label>
                                            </div>
                                            <div class="form-check">
                                              <input class="form-check-input" type="radio" value="" name="desnudo" id="flexRadioDefault2" checked>
                                              <label class="form-check-label text-dark" for="flexRadioDefault2">
                                                No
                                              </label>
                                            </div>
                                            <div class="validation"></div>
                                          </div>
                                        </div> 
                                        <div class="col-md-12"><br>
                                          <button type="submit" class="btn btn-dark" style="float:right;">Buscar&nbsp;&nbsp;<i class="fa fa-search"></i></button>
                                        </div>                          
                                      </div>
                                    </div>
                                  </div>
                                  
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

     <!-- XXXXXXXXXXX
     XXXXXXXXX -->


      <div class="row text-center">
        <p class="col-sm-12 text-dark" style="font-size:20px;">
          <?php echo $mensaje; ?>
        </p>
      </div>
      <div class="row">
        <!-- Rows -->
        <?php if($totalRows_actores != 0){ do { ?>
        <div class="col-md-3">
          <div class="work-box">
            <a target="_blank" href="info-actor.php?actorId=<?php echo $row_actores['id']; ?>" class="text-light">
              <div class="col-md-12 bg-secondary p-2 text-center">
                Ver información &nbsp; <i class="fa fa-eye"></i>
              </div>
            </a>
            <a href="
            <?php
            $id_usuario = $row_actores['id'];
            $query_imagenes = "SELECT * FROM imagenes WHERE usuario_id = '$id_usuario' AND img_pred = 1";
            $imagenes = mysqli_query($connection, $query_imagenes);
            $row_imagenes = mysqli_fetch_array($imagenes, MYSQLI_ASSOC);
            $totalRows_imagenes = mysqli_num_rows($imagenes);
            if($totalRows_imagenes > 0){
              echo $fileDir.$id_usuario.'/'.$row_imagenes['imagen'];
            }else{
              echo 'img/no-image-user.png';
            }
            
            ?>
            " data-lightbox="gallery-mf">
              <div class="work-img cat-size">
                <img src="
                
                <?php
                $id_usuario = $row_actores['id'];
                $query_imagenes = "SELECT * FROM imagenes WHERE usuario_id = '$id_usuario' AND img_pred = 1";
                $imagenes = mysqli_query($connection, $query_imagenes);
                $row_imagenes = mysqli_fetch_array($imagenes, MYSQLI_ASSOC);
                $totalRows_imagenes = mysqli_num_rows($imagenes);
                if($totalRows_imagenes > 0){
                  echo $fileDir.$id_usuario.'/'.$row_imagenes['imagen'];
                }else{
                  echo 'img/no-image-user.png';
                }
                ?>" alt="" class="img-fluid">
              </div>
              <div class="work-content">
                <div class="row">
                  <div class="col-sm-12">
                    <h6>Id: <strong><?php echo $row_actores['id']; ?></strong></h6>
                    <h2 class="w-title"><?php echo $row_actores['nombre'].' '.$row_actores['apellido_pat'].' '.$row_actores['apellido_mat']; ?></h2>
                    <div class="w-more">
                      <span class="w-ctegory"><?php echo $row_actores['edad'].' años'; ?></span> / <span class="w-date"><?php echo $row_actores['estatura'].' cm'; ?></span>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
        <?php } while ($row_actores = mysqli_fetch_assoc($actores)); }  ?>
      </div><br><br><br><br>
    </div>
  </section>
    
  

  <!--/ Section Portfolio End /-->

  <!--/ Section Contact-footer End /-->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader" align="center" class="justify-content-center">
    <img src="img/logos/logox2.png" style="margin-top:40%;" width="200px" height="140px" alt="">
  </div>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/popper/popper.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/counterup/jquery.waypoints.min.js"></script>
  <script src="lib/counterup/jquery.counterup.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <script src="lib/typed/typed.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>
  
</body>
</html>
